import Vue from 'vue'
import Router from 'vue-router'

import Layout from '@/components/layout/Layout'
import Index from '@/pages/Index'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Layout,
      children:[
        {
          path:'',
          name: 'index',
          component:Index
        }
      ]
    }
  ]
})
